<?php
namespace Magenest\Salesforce\Controller\Adminhtml\Queue;

use Magenest\Salesforce\Model\Queue;
use Magenest\Salesforce\Model\QueueFactory;
use Magento\Backend\App\Action\Context;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Setup\Exception;

/**
 * Class Account
 * @package Magenest\Salesforce\Controller\Adminhtml\Queue
 */
class Account extends \Magento\Backend\App\Action
{
    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var ScopeConfigInterface
     */
    protected $_configInterface;

    /**
     * @var Config
     */
    protected $_config;

    /**
     * @var QueueFactory
     */
    protected $queueFactory;

    /**
     * @var string
     */
    protected $type = Queue::TYPE_ACCOUNT;

    /**
     * Customer constructor.
     * @param Context $context
     * @param CustomerFactory $customerFactory
     * @param Config $config
     * @param ScopeConfigInterface $configInterface
     * @param QueueFactory $queueFactory
     */
    public function __construct(
        Context $context,
        CustomerFactory $customerFactory,
        Config $config,
        ScopeConfigInterface $configInterface,
        QueueFactory $queueFactory
    ) {
        $this->queueFactory = $queueFactory;
        $this->_config = $config;
        $this->_configInterface = $configInterface;
        $this->customerFactory = $customerFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        if ($this->getRequest()->isAjax()) {
            $customerCollection = $this->customerFactory->create()->getCollection();
            try {
                foreach ($customerCollection as $customer) {
                    $queue = $this->queueFactory->create();
                    if (!$queue->queueExisted($this->type, $customer->getId())) {
                        $queue->enqueue($this->type, $customer->getId());
                    }
                }
                $this->getResponse()->setBody(json_encode([
                    'error' => 0,
                    'message' => __('All Accounts have been added to queue.')
                ]));
                return;
            } catch (\Exception $e) {
                $this->getResponse()->setBody(json_encode([
                    'error' => 1,
                    'message' => __('Something went wrong while adding Accounts to queue. Error: '.$e->getMessage())
                ]));
                return;
            }
        } else
            return $this->_redirect('*/*/index');
//        $customers = $this->customerFactory->create()->getCollection();
//        /** @var \Magento\Customer\Model\Customer $customer */
//        foreach ($customers as $customer) {
//            $queue = $this->queueFactory->create();
//            if (!$queue->queueExisted($this->type, $customer->getId())) {
//                $queue->enqueue($this->type, $customer->getId());
//            }
//        }
//        $this->messageManager->addSuccess(
//            __('All Accounts have been added to queue, you can delete items you do not want to sync or click Sync Now')
//        );
//        $this->getResponse()->sendResponse('its me');
//        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
//        $resultRedirect->setUrl($this->getUrl('*/*/index'));
//        return $resultRedirect;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Salesforce::config_salesforce');
    }
}
