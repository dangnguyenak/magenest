<?php
namespace Magenest\Salesforce\Controller\Adminhtml\Queue;

use Magenest\Salesforce\Model\Queue;
use Magenest\Salesforce\Model\QueueFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Opportunity
 * @package Magenest\Salesforce\Controller\Adminhtml\Sync
 */
class Opportunity extends \Magento\Backend\App\Action
{
    /**
     * @var
     */
    protected $orderFactory;

    /**
     * @var QueueFactory
     */
    protected $queueFactory;

    /**
     * @var string
     */
    protected $type = Queue::TYPE_OPPORTUNITY;

    /**
     * Opportunity constructor.
     * @param Context $context
     * @param OrderFactory $orderFactory
     * @param QueueFactory $queueFactory
     */
    public function __construct(
        Context $context,
        OrderFactory $orderFactory,
        QueueFactory $queueFactory
    ) {
        $this->queueFactory = $queueFactory;
        $this->orderFactory = $orderFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        if ($this->getRequest()->isAjax()) {
            try {
                $orders = $this->orderFactory->create()->getCollection();
                foreach ($orders as $order) {
                    $queue = $this->queueFactory->create();
                    if (!$queue->queueExisted($this->type, $order->getIncrementId())) {
                        $queue->enqueue($this->type, $order->getIncrementId());
                    }
                }
                $this->getResponse()->setBody(json_encode([
                    'error' => 0,
                    'message' => __('All Opportunities have been added to queue.'),
                ]));
                return;
            } catch (\Exception $e) {
                $this->getResponse()->setBody(json_encode([
                    'error' => 0,
                    'message' => __('Something went wrong while adding record(s) to queue. Error: '.$e->getMessage()),
                ]));
                return;
            }
        } else {
            return $this->_redirect('*/*/index');
        }
//        $orders = $this->orderFactory->create()->getCollection();
//        /** @var \Magento\Sales\Model\Order $order */
//        foreach ($orders as $order) {
//            $queue = $this->queueFactory->create();
//            if (!$queue->queueExisted($this->type, $order->getIncrementId())) {
//                $queue->enqueue($this->type, $order->getIncrementId());
//            }
//        }
//        $this->messageManager->addSuccess(
//            __('All Opportunities have been added to queue, you can delete items you do not want to sync or click Sync Now')
//        );
//        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
//        $resultRedirect->setUrl($this->getUrl('*/*/index'));
//        return $resultRedirect;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Salesforce::config_salesforce');
    }
}
