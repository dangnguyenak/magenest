<?php

namespace Magenest\Salesforce\Controller\Adminhtml\Map;

use Magenest\Salesforce\Controller\Adminhtml\Map as MapController;

class NewMapping extends MapController
{


    public function execute()
    {
        $resultPage = $this->_initAction();
        $resultPage->getConfig()->getTitle()->prepend(__('New Mapping'));
        return $resultPage;
    }

}