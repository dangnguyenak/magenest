<?php
namespace Magenest\Salesforce\Model\Sync;

use Magenest\Salesforce\Model\QueueFactory;
use Magenest\Salesforce\Model\RequestLogFactory;
use Magento\Framework\App\Config\ScopeConfigInterface as ScopeConfigInterface;
use Magento\Config\Model\ResourceModel\Config as ResourceModelConfig;
use Magenest\Salesforce\Model\ReportFactory as ReportFactory;
use Magenest\Salesforce\Model\Connector;
use Magenest\Salesforce\Model\Data;
use Magento\Sales\Model\Order as OrderModel;
use Magento\Sales\Model\OrderFactory;

class Opportunity extends Connector
{
    const SALESFORCE_OPPORTUNITY_ATTRIBUTE_CODE = 'salesforce_opportunity_id';

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \Magenest\Salesforce\Model\Sync\Account
     */
    protected $_account;

    /**
     * @var \Magenest\Salesforce\Model\Sync\Contact
     */
    protected $_contact;

    /**
     * @var \Magenest\Salesforce\Model\Sync\Product
     */
    protected $_product;

    /**
     * @var Job
     */
    protected $_job;

    protected $existedOrders = [];

    protected $createOpportunityIds = [];

    protected $updateOpportunityIds = [];

    protected $dataGetter;

    /**
     * Order constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param ResourceModelConfig $resourceConfig
     * @param ReportFactory $reportFactory
     * @param Data $data
     * @param OrderFactory $orderFactory
     * @param Account $account
     * @param Contact $contact
     * @param Product $product
     * @param Job $job
     * @param DataGetter $dataGetter
     * @param QueueFactory $queueFactory
     * @param RequestLogFactory $requestLogFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ResourceModelConfig $resourceConfig,
        ReportFactory $reportFactory,
        Data $data,
        OrderFactory $orderFactory,
        Account $account,
        Contact $contact,
        Product $product,
        Job $job,
        DataGetter $dataGetter,
        QueueFactory $queueFactory,
        RequestLogFactory $requestLogFactory
    ) {
        parent::__construct($scopeConfig, $resourceConfig, $reportFactory, $queueFactory, $requestLogFactory);
        $this->_orderFactory  = $orderFactory;
        $this->_account = $account;
        $this->_contact = $contact;
        $this->_product = $product;
        $this->_data    = $data;
        $this->_type    = 'Opportunity';
        $this->_table   = 'opportunity';
        $this->_job = $job;
        $this->dataGetter = $dataGetter;
    }

    /**
     * @param $increment_id
     * @return string
     */
    public function sync($increment_id)
    {
        /** @var OrderModel $model */
        $model      = $this->_orderFactory->create()->loadByIncrementId($increment_id);
        $date       = date('Y-m-d', strtotime($model->getCreatedAt()));

        $params = $this->_data->getOpportunity($model, $this->_type);

        $params = array_merge(
            $params,
            [
            'CloseDate'     => $date,
            'Name' => $model->getIncrementId(),
            'StageName'        => 'Prospecting',
            ]
        );

        if ($this->isNewOpportunity($model)) {
            $opportunityId = $this->createRecords($this->_type, $params, $model->getIncrementId());
            $this->saveAttribute($model, $opportunityId);
        } else {
            $opportunityId = $model->getData(self::SALESFORCE_OPPORTUNITY_ATTRIBUTE_CODE);
            $this->updateRecords($this->_type, $opportunityId, $params, $model->getIncrementId());
        }

        return $opportunityId;
    }

    /**
     * @param OrderModel $order
     * @return bool
     */
    public function isNewOpportunity($order)
    {
        if ($order->getData(self::SALESFORCE_OPPORTUNITY_ATTRIBUTE_CODE)) {
            return false;
        }
        return true;
    }

    public function syncAllOpportunities()
    {
        try {
            $orders = $this->_orderFactory->create()->getCollection();
            $lastOrderId = $orders->getLastItem()->getId();
            $count = 0;
            $response = [];
            /** @var \Magento\Sales\Model\Order $order */
            foreach ($orders as $order) {
                $this->addRecord($order->getIncrementId());
                $count++;
                if ($count >= 10000 || $order->getId() == $lastOrderId) {
                    $response += $this->syncQueue();
                }
            }
            return $response;
        } catch (\Exception $e) {
            \Magento\Framework\App\ObjectManager::getInstance()->get(\Psr\Log\LoggerInterface::class)->debug($e->getMessage());
        }
        return null;
    }

    /**
     * @param string $orderIncrementId
     */
    public function addRecord($orderIncrementId)
    {
        /** @var OrderModel $order */
        $order = $this->_orderFactory->create()->loadByIncrementId($orderIncrementId);
        if ($this->isNewOpportunity($order)) {
            $this->addToCreateOpportunityQueue($orderIncrementId);
        } else {
            $this->addToUpdateOpportunityQueue($orderIncrementId, $order->getData(self::SALESFORCE_OPPORTUNITY_ATTRIBUTE_CODE));
        }
    }

    public function syncQueue()
    {
        $createOpportunityResponse = $this->createOpportunities();
        $this->saveAttributes($this->createOpportunityIds, $createOpportunityResponse);
        $updateOpportunityResponse = $this->updateOpportunities();
        $this->saveAttributes($this->updateOpportunityIds, $updateOpportunityResponse);
        $response = $createOpportunityResponse + $updateOpportunityResponse;
        $this->unsetCreateOpportunityQueue();
        $this->unsetUpdateOpportunityQueue();
        return $response;
    }


    protected function addToCreateOpportunityQueue($orderIncrementId)
    {
        $this->createOpportunityIds[] = ['mid' => $orderIncrementId];
    }

    protected function unsetCreateOpportunityQueue()
    {
        unset($this->createOpportunityIds);
        $this->createOpportunityIds = [];
    }

    protected function addToUpdateOpportunityQueue($orderIncrementId, $salesforceId)
    {
        $this->updateOpportunityIds[] = ['mid' => $orderIncrementId, 'sid' => $salesforceId];
    }

    protected function unsetUpdateOpportunityQueue()
    {
        unset($this->updateOpportunityIds);
        $this->updateOpportunityIds = [];
    }

    protected function createOpportunities()
    {
        $response = [];
        if (count($this->createOpportunityIds) > 0) {
            $response = $this->sendOpportunitiesRequest($this->createOpportunityIds, 'insert');
        }
        return $response;
    }

    protected function updateOpportunities()
    {
        $response = [];
        if (count($this->updateOpportunityIds) > 0) {
            $response = $this->sendOpportunitiesRequest($this->updateOpportunityIds, 'update');
        }
        return $response;
    }

    protected function sendOpportunitiesRequest($opportunityIds, $operation)
    {
        $params = [];
        foreach ($opportunityIds as $id) {
            /** @var OrderModel $order */
            $order = $this->_orderFactory->create()->loadByIncrementId($id['mid']);
            $date = date('Y-m-d', strtotime($order->getCreatedAt()));
            $info = $this->_data->getOpportunity($order, $this->_type);
            $info = array_merge(
                $info,
                [
                    'CloseDate'     => $date,
                    'Name' => $order->getIncrementId(),
                    'StageName'        => 'Prospecting',
                ]
            );
            if (isset($id['sid'])) {
                $info += ['Id' => $id['sid']];
            }
            $params[] = $info;
        }
        $response = $this->_job->sendBatchRequest($operation, $this->_type, json_encode($params));
        $this->saveReports($operation, $this->_type, $response, $opportunityIds);
        return $response;
    }

    /**
     * @param $orderIds
     * @param $response
     * @throws \Exception
     */
    protected function saveAttributes($orderIds, $response)
    {
        if (is_array($response) && is_array($orderIds)) {
            for ($i=0; $i<count($orderIds); $i++) {
                $order = $this->_orderFactory->create()->loadByIncrementId($orderIds[$i]['mid']);
                if (isset($response[$i]['id']) && $order->getId()) {
                    $this->saveAttribute($order, $response[$i]['id']);
                }
            }
        } else {
            throw new \Exception('Response not an array');
        }
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param $salesforceId
     */
    protected function saveAttribute($order, $salesforceId)
    {
        $resource = $order->getResource();
        $order->setData(self::SALESFORCE_OPPORTUNITY_ATTRIBUTE_CODE, $salesforceId);
        $resource->saveAttribute($order, self::SALESFORCE_OPPORTUNITY_ATTRIBUTE_CODE);
    }
}
