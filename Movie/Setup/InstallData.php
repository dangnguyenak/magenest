<?php

namespace Magenest\Movie\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * Install data for table magenest_movie
         */
        $installer = $setup;
        $installer->startSetup();

        $connection = $installer->getConnection();
        $data_movie = [
                [
                    'name' => 'Avartar 2008',
                    'description'=>'Avartar 2008',
                    'rating'=>4,
                    'director_id'=>1
                ],
                [
                    'name'=>'Kill Switch',
                    'description'=>'Kill switch',
                    'rating'=>3,
                    'director_id'=>2
                ],
                [
                    'name'=>'Transformer',
                    'description'=>'Transformer 1 2 3 4 5',
                    'rating'=>5,
                    'director_id'=>4
                ],
                [
                    'name'=>'Terminator',
                    'description'=>'Terminaltor 1 2 3 4',
                    'rating'=>4,
                    'director_id'=>3
                ]
            ];
        foreach ($data_movie as $movie)
         {
            $connection->insertForce($installer->getTable('magenest_movie'), $movie);
         }

        /*
         *   Install data for table magenest_director
         */
        $data_director = [
                ['name'=>'Micheal Bay'],
                ['name'=>'Johnny Tri Nguyen '],
                ['name'=>'James Cameron'],
                ['name'=>'John Weapon']
            ];
        foreach ($data_director as $director)
        {
            $connection->insertForce($installer->getTable('magenest_director'), $director);
        }

        /*
         * Install data for table magenest_actor
         */

        $data_actor = [
            ['name'=>'Jason Statham'],
            ['name'=>'Jet Li'],
            ['name'=>'Scarlett Johansson'],
            ['name'=>'Robert Downey Jr.'],
            ['name'=>'Mark Ruffalo'],
            ['name'=>'Chris Evans'],
            ['name'=>'Jeremy Renner']
        ];

        foreach ($data_actor as $actor)
        {
            $connection->insertForce($installer->getTable('magenest_actor'), $actor);
        }

        /*
         * Install data for table magenest_movie_actor

        $data_movie_actor = [
            [
                'movie_id'=>
            ]
        ];
        */
        $installer->endSetup();
    }
}