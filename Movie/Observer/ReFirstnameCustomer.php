<?php
namespace Magenest\Movie\Observer;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Event\ObserverInterface;

class ReFirstnameCustomer implements ObserverInterface
{
    /** @var CustomerRepositoryInterface */
    protected $customerRepository;

    /**
     * @param CustomerRepositoryInterface $customerRepository
     */
//    public function __construct(
//        CustomerRepositoryInterface $customerRepository
//    )
//    {
//        $this->customerRepository = $customerRepository;
//    }
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // TODO: Implement execute() method.
        $customer = $observer->getCustomer();
        $customer->setFirstname('Magenest');

    }
}