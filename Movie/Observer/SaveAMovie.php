<?php
namespace Magenest\Movie\Observer;

use Magento\Framework\Event\ObserverInterface;

class SaveAMovie implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // TODO: Implement execute() method.
        $movie = $observer->getData('movieModel');
        $movie->setRating('0');
    }
}