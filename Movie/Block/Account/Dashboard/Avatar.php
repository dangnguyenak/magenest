<?php
namespace Magenest\Movie\Block\Account\Dashboard;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Customer\Api\CustomerMetadataInterface;
use Magento\Framework\ObjectManagerInterface;

class Avatar extends \Magento\Framework\View\Element\Template
{
    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $viewFileUrl;

    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $customer;


    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    protected $currentCustomer;

    /**
     * Avatar constructor.
     * @param Context $context
     * @param ObjectManagerInterface $objectManager
     * @param \Magento\Framework\View\Asset\Repository $viewFileUrl
     * @param \Magento\Customer\Model\Customer $customer
     * @param \Magento\Framework\Filesystem $filesystem
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        ObjectManagerInterface $objectManager,
        \Magento\Framework\View\Asset\Repository $viewFileUrl,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        array $data = [])
    {
        $this->currentCustomer = $currentCustomer;
        $this->objectManager = $objectManager;
        $this->viewFileUrl = $viewFileUrl;
        $this->customer = $customer;
        parent::__construct($context, $data);
    }

    /**
     * @return \Magento\Customer\Api\Data\CustomerInterface|null
     */
    public function getCustomer()
    {
        try {
            return $this->currentCustomer->getCustomer();
        } catch (NoSuchEntityException $e){
            return null;
        }
    }


    /**
     * @param $file
     * @return bool
     */
    public function checkImageFile($file)
    {
        $file = base64_decode($file);
        $mediaDirectory = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA);
        $fileName = CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER . '/' . ltrim($file, '/');
        $path = $mediaDirectory->getAbsolutePath($fileName);
        if (!$mediaDirectory->isFile($fileName)
            && !$this->objectManager->get('Magento\MediaStorage\Helper\File\Storage')->processStorageFile($path)
        ) {
            return false;
        }
        return true;
    }

    /**
     * @param $file
     * @return string
     */
    public function getAvatarCurrentCustomer($file)
    {
        if ($this->checkImageFile(base64_encode($file)) == true) {
            return $this->getUrl('movie/avatar/view/',['image' => base64_encode($file)]);
        }
        return $this->getDefaultAvatar();
    }

    /**
     * @return string
     */
    public function getDefaultAvatar()
    {
        return $this->viewFileUrl->getUrl('Magenest_Movie::images\no-profile-photo.jpg');
    }

    /**
     * @param bool $customer_id
     * @return string
     */
    public function getCustomerAvatarById($customer_id = false)
    {
        if ($customer_id) {
            $customerDetail = $this->customer->load($customer_id);
            if ($customerDetail && !empty($customerDetail->getAvatar)) {
                if ($this->checkImageFile(base64_encode($customerDetail->getAvatar())) == true) {
                    return $this->getUrl('movie/avatar/view/',['image' => base64_encode($customerDetail->getAvatar())]);
                }
            }
        }
        return $this->getDefaultAvatar();
    }
}