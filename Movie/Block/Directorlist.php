<?php
namespace Magenest\Movie\Block;


use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;
use Magenest\Movie\Model\DirectorsFactory;


class Directorlist extends Template
{

    protected $directors;
    public function __construct(Context $context, DirectorsFactory $model)
    {
        $this->directors = $model;
        parent::__construct($context);

    }

    public function getDirectorCollection()
    {
        $directorsCollection = $this->model->getCollection();
        return $directorsCollection;
    }

    public function getDirectors()
    {
        $data = array();

        $collections = $this->directors->create()->getCollection();

        foreach ($collections as $collection)
        {
            $data[] = [
                'value'=>$collection->getId(),
                'label'=> __($collection->getName())
            ];
        }
        return $data;
    }
}