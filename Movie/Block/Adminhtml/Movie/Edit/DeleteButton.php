<?php
namespace Magenest\Movie\Block\Adminhtml\Movie\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

class DeleteButton extends GenericButton implements ButtonProviderInterface
{
    public function getButtonData()
    {
        $data = [];
        if ($this->getId()) {
            $data = [
                'label' => __('Delete'),
                'class' => 'delete',
                'on_click' => 'deleteConfirm(\''
                    .__('Are you want to delete this movie?')
                    . '\', \'' . $this->getDeleteUrl() .'\')',
                'sort_order' => 20,
            ];
        }
        return $data;
        // TODO: Implement getButtonData() method.
    }

    public function getDeleteUrl()
    {
        return $this->getUrl('*/*/delete', ['movie_id' => $this->getId()]);
    }
}