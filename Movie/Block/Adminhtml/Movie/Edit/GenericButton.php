<?php
namespace Magenest\Movie\Block\Adminhtml\Movie\Edit;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;

class GenericButton
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    public function __construct(Context $context, Registry $registry)
    {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->coreRegistry = $registry;
    }

    /**
     * @return int | null
     */
    public function getId()
    {
        $movieModel = $this->coreRegistry->registry('movie');
        return $movieModel ? $movieModel->getId() :null;
    }

    /**
     * @param string $route
     * @param array $params
     * @return string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }

}