<?php
namespace Magenest\Movie\Block\Adminhtml\Movie;

use Magento\Backend\Block\Widget\Form\Container;

class Edit extends Container
{
    protected function _construct()
    {
        $this->_blockGroup = 'Magenest_Movie';
        $this->_controller = 'adminhtml_movie';
        $this->_objectId = 'movie_id';

        parent::_construct();
    }
}