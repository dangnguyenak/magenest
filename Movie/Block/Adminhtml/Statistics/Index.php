<?php
namespace Magenest\Movie\Block\Adminhtml\Statistics;

use Magento\Backend\Block\Template;
use Magento\Framework\Module\ModuleListInterface;

use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory as InvoiceCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory as CreditCollectionFactory;


class Index extends Template
{
    /**
     * @var \Magento\Framework\Module\ModuleListInterface
     */
    protected $_moduleList;

    /**
     * @var CustomerCollectionFactory
     */
    protected $customers;

    /**
     * @var ProductCollectionFactory
     */
    protected $products;

    /**
     * @var OrderCollectionFactory
     */
    protected $orders;

    /**
     * @var InvoiceCollectionFactory
     */
    protected $invoices;

    /**
     * @var CreditCollectionFactory
     */
    protected $creditmemos;

    /**
     * Index constructor.
     * @param Template\Context $context
     * @param ModuleListInterface $moduleList
     * @param CustomerCollectionFactory $customerCollectionFactory
     * @param ProductCollectionFactory $productCollectionFactory
     * @param OrderCollectionFactory $orderCollectionFactory
     * @param InvoiceCollectionFactory $invoiceCollectionFactory
     * @param CreditCollectionFactory $creditCollectionFactory
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        ModuleListInterface $moduleList,
        CustomerCollectionFactory $customerCollectionFactory,
        ProductCollectionFactory $productCollectionFactory,
        OrderCollectionFactory $orderCollectionFactory,
        InvoiceCollectionFactory $invoiceCollectionFactory,
        CreditCollectionFactory $creditCollectionFactory,
        array $data = [])
    {
        $this->_moduleList = $moduleList;
        $this->products = $productCollectionFactory;
        $this->customers = $customerCollectionFactory;
        $this->orders = $orderCollectionFactory;
        $this->invoices = $invoiceCollectionFactory;
        $this->creditmemos =$creditCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return array
     */
    public function getModuleList()
    {
        return $this->_moduleList->getAll();
    }

    /**
     * @return array
     */
    public function getModuleNotMagento()
    {
        $moduleNotMagento = [];
        $modules = $this->_moduleList->getNames();
        foreach ($modules as $module) {
            if (!preg_match('/^Magento_/', $module)) {
                $moduleNotMagento [] = $module;
            }
        }

        return $moduleNotMagento;
    }

    public function getCustomerList()
    {
        return $this->customers->create()->getData();
    }

    public function getProductList()
    {
        return $this->products->create()->getData();
    }

    public function getOrderList()
    {
        return $this->orders->create()->getData();
    }

    public function getInvoiceList()
    {
        return $this->invoices->create()->getData();
    }

    public function getCreditList()
    {
        return $this->creditmemos->create()->getData();
    }
}