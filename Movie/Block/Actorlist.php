<?php
namespace Magenest\Movie\Block;


use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;
use Magenest\Movie\Model\Actors;


class Actorlist extends Template
{

    public function __construct(Context $context, Actors $model)
    {
        $this->model = $model;
        parent::__construct($context);

    }

    /**
     * [getActorCollection description]
     * @return ActorsCollection
     */
    public function getActorCollection()
    {
        $actorCollection = $this->model->getCollection();
        return $actorCollection;
    }
}