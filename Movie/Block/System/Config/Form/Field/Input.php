<?php
namespace Magenest\Movie\Block\System\Config\Form\Field;

use \Magento\Framework\Data\Form\Element\AbstractElement;
use \Magento\Backend\Block\Template\Context;
/*
 * Use custom Model
 */
use \Magenest\Movie\Model\Movies;
use \Magenest\Movie\Model\Actors;

class Input extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $_modelMovieFactory;
    protected $_modelActorFactory;

    public function __construct(
        Context $context,
        Movies $modelMovieFactory,
        Actors $modelActorFactory,
        array $data = [])
    {
        $this->_modelActorFactory = $modelActorFactory;
        $this->_modelMovieFactory = $modelMovieFactory;
        parent::__construct($context, $data);
    }

    protected function _getElementHtml(AbstractElement $element)
    {
        $id = $element->getId();
        switch($id) {
            case 'movie_moviepage_movie_movie':
                $this->movie_id($element);
                break;
            case 'movie_moviepage_movie_actor':
                $this->actor_id($element);
                break;
            default:
                return parent::_getElementHtml($element);
        }
        $element->setReadonly(true);

        return parent::_getElementHtml($element);
    }

    public function actor_id(AbstractElement $element)
    {
        $actorCollection = $this->_modelActorFactory->getCollection();
        $getAllActors = $actorCollection->getSize();


        $element->setValue($getAllActors);
    }
    public function movie_id(AbstractElement $element)
    {

        $movieCollection = $this->_modelMovieFactory->getCollection();
        $getAllMovies = $movieCollection->getSize();

        $element->setValue($getAllMovies);
    }
}