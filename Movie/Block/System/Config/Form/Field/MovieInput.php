<?php
namespace Magenest\Movie\Block\System\Config\Form\Field;

use \Magento\Framework\Data\Form\Element\AbstractElement;

class MovieInput extends \Magento\Config\Block\System\Config\Form\Field
{
    protected function _getElementHtml(AbstractElement $element)
    {
        $model = 'Magenest\Movie\Model\Movies';
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $movieCollection = $objectManager->create($model);
        $collection = $movieCollection->getCollection();
        $getAllMovies = $collection->getSize();

        $element->setValue($getAllMovies);
//        $element->setDisabled('disabled');
        $element->setReadonly(true);

        return parent::_getElementHtml($element);
    }
}