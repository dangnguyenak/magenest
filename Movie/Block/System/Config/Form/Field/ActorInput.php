<?php
namespace Magenest\Movie\Block\System\Config\Form\Field;

use \Magento\Framework\Data\Form\Element\AbstractElement;
use \Magento\Backend\Block\Template\Context;
use \Magenest\Movie\Model\Actors;

class ActorInput extends \Magento\Config\Block\System\Config\Form\Field
{

    protected $_modelActorFactory;

    public function __construct(
        Context $context,
        Actors $modelActorFactory,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->_modelActorFactory = $modelActorFactory;
    }

    protected function _getElementHtml(AbstractElement $element)
    {
//        $model = 'Magenest\Movie\Model\Actors';
//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//        $actorCollection = $objectManager->create($model);
//        $collection = $actorCollection->getCollection();
//        $getAllActors = $collection->getSize();


        $actorCollection = $this->_modelActorFactory->getCollection();
        $getAllActors = $actorCollection->getSize();

        $element->setValue($getAllActors);
//        $element->setDisabled(true);
        $element->setReadonly(true);
        return parent::_getElementHtml($element);
    }
}