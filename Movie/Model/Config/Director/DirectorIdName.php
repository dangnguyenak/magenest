<?php
namespace Magenest\Movie\Model\Config\Director;

use Magento\Framework\Option\ArrayInterface;
use Magenest\Movie\Model\ResourceModel\Directors\CollectionFactory;

class DirectorIdName implements ArrayInterface
{
    protected $directorCollectionFactory;

    public function __construct(CollectionFactory $directorCollectionFactory)
    {
        $this->directorCollectionFactory = $directorCollectionFactory;
    }

    public function toOptionArray()
    {
        $directorData = [];
        $directorCollection = $this->directorCollectionFactory->create();
        foreach ($directorCollection->getData() as $item) {
            $directorData [] = [
                'label' => $item['director_id'].' - '.$item['name'],
                'value' => $item['director_id']
            ];
        }


        return $directorData;
    }
}