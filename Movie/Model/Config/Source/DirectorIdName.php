<?php
namespace Magenest\Movie\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Magenest\Movie\Model\ResourceModel\Directors\CollectionFactory;

class DirectorIdName implements ArrayInterface
{
    protected $directorCollectionFactory;

    public function __construct(CollectionFactory $directorCollectionFactory)
    {
        $this->directorCollectionFactory = $directorCollectionFactory;
    }

    public function toOptionArray()
    {
        $directorData = [];
        $directorCollection = $this->directorCollectionFactory->create();
        foreach ($directorCollection->getData() as $item) {
            $directorData [$item['director_id']] = $item['director_id'].' - '.$item['name'];
        }


        return $directorData;
    }
}