<?php
namespace Magenest\Movie\Model\Config;

use Magento\Framework\Option\ArrayInterface;

class Rating implements ArrayInterface
{


    public function toOptionArray()
    {
        return [
            1 => '1',
            2 => '2',
            3 => '3',
            4 => '4',
            5 => '5'
        ];
    }
}