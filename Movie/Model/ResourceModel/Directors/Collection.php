<?php


namespace Magenest\Movie\Model\ResourceModel\Directors;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;


class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Magenest\Movie\Model\Directors',
            'Magenest\Movie\Model\ResourceModel\Directors'
        );
    }
}