<?php


namespace Magenest\Movie\Model\ResourceModel\Movies;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;


class Collection extends AbstractCollection
{
    protected $_idFieldName = 'movie_id';
    protected function _construct()
    {
        $this->_init(
            'Magenest\Movie\Model\Movies',
            'Magenest\Movie\Model\ResourceModel\Movies'
        );
    }
}