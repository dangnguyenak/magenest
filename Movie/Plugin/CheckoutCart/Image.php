<?php

namespace Magenest\Movie\Plugin\CheckoutCart;


use Magento\Catalog\Helper\Image as ImageHelper;
use Magento\Catalog\Model\ProductFactory;
use Magento\Swatches\Helper\Data;

class Image {
    protected $productFactory;

    protected $imageHelper;

    protected $swatchDataHelper;

    public function __construct(
        ImageHelper $imageHelper,
        ProductFactory $productFactory,
        Data $swatchDataHelper
    )
    {
        $this->swatchDataHelper = $swatchDataHelper;
        $this->productFactory = $productFactory;
        $this->imageHelper = $imageHelper;
    }

    public function afterGetImage(\Magento\Checkout\Block\Cart\Item\Renderer $subject, $result)
    {
        $item = $subject->getItem();
        $product = $this->productFactory->create();
        $product->load(key($item->getData('qty_options')));
        if ($this->swatchDataHelper->isProductHasSwatch($subject->getProduct()))
        {
            $imageUrl = $this->imageHelper->init($product, 'cart_page_product_thumbnail')->getUrl();
            $result->setImageUrl($imageUrl);
        }


        return $result;
    }
}