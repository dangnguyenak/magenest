<?php

namespace Magenest\Movie\Plugin\Minicart;

use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Helper\Image as ImageHelper;
use Magento\Swatches\Helper\Data;

class Image
{
    protected $swatchDataHelper;

    protected $productFactory;

    protected $imageHelper;

    public function __construct(
        ProductFactory $productFactory,
        ImageHelper $imageHelper,
        Data $swatchDataHelper
    )
    {
        $this->swatchDataHelper = $swatchDataHelper;
        $this->productFactory = $productFactory;
        $this->imageHelper = $imageHelper;
    }

    public function aroundGetItemData($subject, $proceed, $item)
    {
        $result = $proceed($item);
        $product = $this->productFactory->create()->load(key($item->getData('qty_options')));
        if ($this->swatchDataHelper->isProductHasSwatch($item->getProduct()))
        {
            $result['product_name'] = $item->getData('sku');
            $imageUrl = $this->imageHelper->init($product, 'product_small_image')->getUrl();
            $result['product_image']['src'] = $imageUrl;
        }

        return $result;
    }
}