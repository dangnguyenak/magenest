<?php
namespace Magenest\Movie\Controller\Adminhtml\Directorlist;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    protected $resultPageFactory;

    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        // TODO: Implement execute() method.
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_Movie::directorlist');
        $resultPage->addBreadcrumb(__('Directors list'), __('Directors list'));
        $resultPage->addBreadcrumb(__('Manage Directors list'), __('Manage Directors list'));

        $resultPage->getConfig()->getTitle()->prepend(__('Directors list'));

        return $resultPage;
    }
}