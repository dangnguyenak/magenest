<?php
namespace Magenest\Movie\Controller\Adminhtml\Movie;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;

    public function __construct(Context $context,PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        // TODO: Implement execute() method.
        $resultPage = $this->resultPageFactory->create();

        $resultPage->setActiveMenu('Magenest_Movie::movie');
        $resultPage->addBreadcrumb(__('Magenest Movies'), __('Magenest Movies'));
        $resultPage->addBreadcrumb(__('Movies list'), __('Movies list'));
        $resultPage->getConfig()->getTitle()->prepend(__('Movies'));



        return $resultPage;
    }


}