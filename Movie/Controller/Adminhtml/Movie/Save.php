<?php
namespace Magenest\Movie\Controller\Adminhtml\Movie;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magenest\Movie\Model\MoviesFactory;
use Magento\Setup\Exception;

class Save extends Action
{
    protected $_coreRegistry;

    protected $_resultPageFactory;

    protected $movieFactory;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        Registry $registry,
        MoviesFactory $moviesFactory
    )
    {
        $this->_coreRegistry = $registry;
        $this->_resultPageFactory = $pageFactory;
        $this->movieFactory = $moviesFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $formData = $this->getRequest()->getPostValue();
        if ($formData) {
            $movieId = !empty($formData['movie_id']) ? $formData['movie_id'] : null;
            $movieModel = $this->movieFactory->create();
            if (!$movieId) {
                unset($formData['movie_id']);
            } else {
                $movieModel->load($movieId);
            }

            $movieModel->setData($formData);

            try {
                $movieModel->save();
                $this->messageManager->addSuccess(__('The movie has been saved successfully.'));
                $this->_getSession()->setMovieData(false);
                return $this->_redirect('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the movie.'));

                $this->_getSession()->setMovieData($formData);
                if (!empty($formData['movie_id'])) {
                    return $this->_redirect('*/*/edit',['id' => $movieModel->getId()]);
                } else {
                    return $this->_redirect('*/*/add');
                }
            }
        }
        return $this->_redirect('*/*/');
    }


}