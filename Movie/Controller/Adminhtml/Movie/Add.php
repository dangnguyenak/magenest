<?php
namespace Magenest\Movie\Controller\Adminhtml\Movie;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;

class Add extends Action
{
    public function execute()
    {
        $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $this->_forward('edit');
        // TODO: Implement execute() method.
    }
}