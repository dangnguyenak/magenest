<?php
namespace Magenest\Movie\Controller\Adminhtml\Movie;

use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;
use Magenest\Movie\Model\MoviesFactory;

class Edit extends Action
{
    /**
     * @var \Magenest\Movie\Model\MoviesFactory
     */
    protected $movieFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * Edit constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     */
    public function __construct(
        Action\Context $context,
        PageFactory $pageFactory,
        Registry $registry,
        MoviesFactory $moviesFactory)
    {
        $this->coreRegistry = $registry;
        $this->movieFactory = $moviesFactory;
        $this->_resultPageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('movie_id');
        $movieModel = $this->movieFactory->create();

        if ($id) {
            $movieModel->load($id);
            if (!$movieModel->getId()) {
                $this->messageManager->addError(__('This movie no longer exists.'));
                return $this->_redirect('*/*/');
            }
            $title = 'Edit Movie: '. $movieModel->getName();
        } else {
            $title = 'Add New Movie';
        }

        $this->_session->getFormData(true);
//        $this->_session->

        $this->coreRegistry->register('movie', $movieModel);

        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);


        $resultPage->setActiveMenu('Magenest_Movie::movie');
        $resultPage->getConfig()->getTitle()->prepend(__($title));

        return $resultPage;
        // TODO: Implement execute() method.
    }
}