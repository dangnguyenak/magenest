<?php
namespace Magenest\Movie\Controller\Adminhtml\Statistics;

use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;


class Index extends Action
{


    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('Magenest_Movie::movie');

        $resultPage->addBreadcrumb(__('Statistics'), __('Statistics'));
        $resultPage->addBreadcrumb(__('Report'), __('Report'));
        $resultPage->getConfig()->getTitle()->prepend(__('Request Report'));

        return $resultPage;
    }
}